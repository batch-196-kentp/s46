console.log("hi");

// mock database
let posts = [];

// count variable that will server as the post id
let count = 1;

// Add post data
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	//prevent the page from loading, hinders the default behavior of the event
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// increment the id value in count variable
	count++;
	showPosts(posts);
	alert("Movie successfully added!");
	// console.log(posts);
});

// Show posts
const showPosts = (posts) => {
	console.log(posts)
	let postEntries = '';

	posts.forEach((post) => {
		console.log(post);
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries
};

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;	
};

// const deletePost = (id) => {
// 	let dlt = document.querySelector(`#post-${id}`)
// 	dlt.remove();
// } 
// Update a Post
document.querySelector("#form-edit-post").addEventListener('submit', (e) => {
	
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){

			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Movie post successfully updated');

			break;
		}
	}
});

// ACTIVITY SOLUTION
const deletePost = (id) => {
	for(let i = 0; i < posts.length; i++){
		
		if(posts[i].id.toString() === id){
			
			posts.splice(i, i+1)
			showPosts(posts);
			
			break;
		}
	}
	
};
